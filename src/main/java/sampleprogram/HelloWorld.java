/*
 * Copyright Progressive Leasing LLC.
 */
package sampleprogram;

/**
 * Example class under test.
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @version 1.0
 * @see scr.test.java.sampleprogram.HelloWorldTest.java
 */
public class HelloWorld {
	
	/**
	 * The class has one public method whose sole purpose is 
	 * to print a message to console.
	 */
	public void sayHello() { 
		System.out.println("Bonjour Monde");
	}

}
