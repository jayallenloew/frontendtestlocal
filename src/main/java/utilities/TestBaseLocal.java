package utilities;

import org.openqa.selenium.WebDriver;

/**
 * Superclass, or test base, for functional front end tests with a local driver: run 
 * tests on your machine.
 * <p>
 * Set path to your repo in LocalResources.java.
 * <p>
 * Expect an error if the local driver you choose isn't installed.
 * <p>
 * Use TestBaseSauce instead if you want to run tests in the SauceLabs cloud.
 * <p> 
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @version 1.0
 * @see LocalResources.java
 * @see TestBaseSauceMobile.java
 * @see TestBaseSauceDesktopFactory.java
 */
public class TestBaseLocal {

	/**
	 * Set driver type here. The driver in the child test will be of this type.
	 */
	private DriverTypeLocal driverType = DriverTypeLocal.CHROME;
	
	private WebDriver driver;
	
	public TestBaseLocal() { 
		// no args
	}
	
	/**
	 * Optional, for testability. To loop through supported browser types 
	 * when testing the utility itself. Or to simply override the currently 
	 * declared value above.
	 * <p>
	 * @param driverTypeToTest
	 */
	protected void setDriverTypeForTestClass(DriverTypeLocal driverTypeToTest) { 
		this.driverType = driverTypeToTest;
	}
	protected DriverTypeLocal getDriverTypeForTestClass() { 
		return this.driverType;
	}
	
	/**
	 * Return a ready-to-use WebDriver instance of the requested type.
	 * <p>
	 * @return - An instance of org.openqa.selenium.WebDriver
	 */
	public WebDriver getPreparedDriver() { 
		driver = new CommonDriversLocal(this.driverType).getPreparedDriver();
		return driver;
	}

}
