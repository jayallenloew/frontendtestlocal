package utilities;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;

/**
 * Load an encrypted key from properties file.
 * <p>
 * Use the key decrypt a value and return it to a downstream test.
 * <p>
 * Clean up safely.
 * <p>
 * Your encryption properties file is assumed be in src/test/resources.
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @version 1.0
 */
public class Encryption_B_Decrypt {
	
	private static final Logger LOGGER = Logger.getLogger(Encryption_B_Decrypt.class.getName());

	/**
	 * Change this to the path to your encryption properties file, which you 
	 * should have put in src/test/resources.
	 */
	private String pathToMyKey = "./src/test/resources/encryption_aloew.properties";
		
	/**
	 * An instance of this Jasypt class is required for encryption or decryption.
	 */
	private StandardPBEStringEncryptor spbesEncryptor;
	
	/**
	 * An instance of Properties is required to read from or write to properties.
	 */
	private Properties props;

	/**
	 * An instance of InputStream is required to read from file.
	 */
	private InputStream streamIn;

	/**
	 * A single constructor takes no arguments and loads the properties.
	 */
	public Encryption_B_Decrypt() { 
		props = new Properties();
		try {
			streamIn = new FileInputStream(this.pathToMyKey);
		} catch (FileNotFoundException fNF) {
			LOGGER.warning(fNF.getClass().getSimpleName() + ":\t" 
					+ "error reading from properties file at "
					+ this.pathToMyKey);
		}
		try {
			props.load(streamIn);
		} catch (IOException iOE) {
			LOGGER.warning(iOE.getClass().getSimpleName() + ":\t" 
					+ "error reading from properties file at "
					+ this.pathToMyKey);
		}
		spbesEncryptor = new StandardPBEStringEncryptor();
		spbesEncryptor.setPassword("pgIhTj/+m0Bd7mlNiu1mNRQrzt5t32nR");
		String foo = spbesEncryptor.decrypt("1uD9Dk4JsprOCgEMxED1DztJyGfeVpu2wYzopFcDtSrt+7m3kfm9kW40+i7h5pvA0LT7v3seSBk=");
		System.out.println("foo:\t" + foo);
		
	} 
	
	/**
	 * Decrypt the String passed in, then return it.
	 * 
	 * @param toDecrypt - A plain-text, readable String to be decrypted.
	 * @return The decrypted equivalent of whatever you just passed in.
	 */
	public String getDecryptedValue(String toDecrypt) { 
		return spbesEncryptor.decrypt(toDecrypt);
	}
	


}
