package utilities;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Set;

public class ServiceTests
{
    private static final String environment = "13";     //RC, QA8, QA13
    private static String sid = "";
    private static String leaseId = "";

    public static String getBearer() {

        StringBuilder response = new StringBuilder();
        URL url;

        try {
            if (environment.equals("RC")) {
                url = new URL("https://slc-rcpwebpws.stormwind.local/eComAPI/auth/credentials");
            } else {
                url = new URL("http://vdc-qaswebapp12.stormwind.local/eComAPI/auth/credentials");
            }

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            String query = "{\n" +
                           "  \"username\":\"{?p$maLz}2QZ56Za]rqyG\",\n" +
                           "  \"password\":\"T^g$7~7N4wxEtphn{iCK\",\n" +
                           "  \"rememberMe\":true\n" +
                           "}";
            connection.setRequestProperty("Content-Length", Integer.toString(query.length()));
            connection.setDoOutput(true);
            connection.getOutputStream().write(query.getBytes(StandardCharsets.UTF_8));
            connection.setReadTimeout(8 * 1000);
            connection.connect();

            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String line;
            String [] res;

            while ((line = reader.readLine()) != null) {
                if (line.contains("bearerToken")) {
                    res = line.split("\"");
                    response.append(res[15]);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response.toString();
    }

    public static String getSessionInitial() {

        StringBuilder response = new StringBuilder();
        URL url;

        try {
            if (environment.equals("RC")) {
                url = new URL("https://slc-rcpwebpws.stormwind.local/eComAPI/v1/session");
            } else {
                url = new URL("http://vdc-qaswebapp" + environment + ".stormwind.local/eComAPI/v1/session");
            }

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Authorization", "Basic dGVzdG1lcmNoYW50MTp0ZXN0MTIz");
            String query = "{}";
            connection.setRequestProperty("Content-Length", Integer.toString(query.length()));
            connection.setDoOutput(true);
            connection.getOutputStream().write(query.getBytes(StandardCharsets.UTF_8));
            connection.getOutputStream().close();
            connection.setReadTimeout(8 * 1000);
            connection.getResponseCode();

            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String line;
            String [] res;

            while ((line = reader.readLine()) != null) {
                if (line.contains("sessionId")) {
                    res = line.split("\"");
                    response.append(res[3]);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response.toString();
    }

	public static String getSessionCookie(Set cookies) {

        String sessionCookiesString = cookies.toString();
        String[] sessionCookiesArray = sessionCookiesString.split("sessionId=");
        String[] sessionCookie = sessionCookiesArray[1].split(";");
        return sessionCookie[0];
    }

    /**
     * Can use Prog.Token to update the SID with new order items.
     * Setup is assuming only one item in cart.
     * @param orderAmount
     */
    public static void putOrder(String sessionId, int orderAmount) {

        URL url;
        String query = "{\n" +
                               "\"sessionId\": \"" + sessionId + "\",\n" +
                               "\"customerEmail\": \"leroy.jenkins@progleasing.com\",\n" +
                               "\"billingAddress\": {},\n" +
                               "\"orderAmount\": \"" + orderAmount + "\",\n" +
                               "\"orderTaxAmount\": 0,\n" +
                               "\"orderLines\": [\n" +
                               "    {\n" +
                               "        \"sku\": \"MACU12312\",\n" +
                               "        \"name\": \"iPhone X\",\n" +
                               "        \"quantity\": 1,\n" +
                               "        \"unitPrice\": \"" + orderAmount + "\",\n" +
                               "        \"productUrl\": \"https://cdn.macrumors.com/article-new/2017/09/iphonexdesign-1-800x597.jpg\",\n" +
                               "        \"imageUrl\": \"https://cdn.macrumors.com/article-new/2017/09/iphonexdesign-1-800x597.jpg\",\n" +
                               "        \"used\": false\n" +
                               "    }\n" +
                               "  ],\n";

        try {
            if (environment.equals("RC")) {
                url = new URL("https://slc-rcpwebpws.stormwind.local/eComAPI/v1/session");
                query = query + "\"progressiveStoreId\": 93844\n" +
                                "}";
            } else {
                url = new URL("http://vdc-qaswebapp"+ environment +".stormwind.local/eComAPI/v1/session");
                query = query + "\"progressiveStoreId\": 82911\n" +
                                "}";
            }

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            connection.setRequestMethod("PUT");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Authorization", "Basic dGVzdG1lcmNoYW50MTp0ZXN0MTIz");
            connection.setRequestProperty("Content-Length", Integer.toString(query.length()));
            connection.setDoOutput(true);
            connection.getOutputStream().write(query.getBytes(StandardCharsets.UTF_8));
            connection.getOutputStream().close();
            connection.setReadTimeout(8 * 1000);
            connection.getResponseCode();

        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public static void postSubmit() {
        
    }

    public static void validateText(String sessionId, String textToValidate, String endpoint) {

        String[] expected = textToValidate.split(",");
        String response = "";

        switch (endpoint.toLowerCase()) {
            case "getpayment":
                response = getPayment(sessionId);
                break;

            case "getapplication":
                response = getApplication(sessionId);
                break;

            case "getlease":
                response = getLease(sessionId);
                break;

            case "getcart":
                response = getCart(sessionId);
                break;
        }

//        System.out.println(response);

        for (int i = 0; i < expected.length; i++) {
            if (response.indexOf(expected[i]) != -1) {
                System.out.println("Found text \"" + expected[i] + "\" in " + sessionId + " " + endpoint + "() response text.");
            } else {
                System.out.println("Did not find text \"" + expected[i] + "\" in " + sessionId + " " + endpoint + "() response text.");
            }
        }
    }

    public static String getPayment(String sessionId) {

        StringBuilder response = new StringBuilder();
        URL url;

        try {
            if (environment.equals("RC")) {
                url = new URL("https://slc-rcpwebpws.stormwind.local/eComAPI/v1/session/" + sid + "/payment");
            } else {
                url = new URL("http://vdc-qaswebapp" + environment + ".stormwind.local/eComAPI/v1/session/" + sid + "/payment");
            }

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Authorization", "Bearer " + getBearer());
            connection.setReadTimeout(8*1000);
            connection.getResponseCode();

            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String line;

            while ((line = reader.readLine()) != null) {
                response.append(line + "\n");
            }

            reader.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return response.toString();
    }

    public static String getApplication(String sessionId) {

        StringBuilder response = new StringBuilder();
        URL url;

        try {
            if (environment.equals("RC")) {
                url = new URL("https://slc-rcpwebpws.stormwind.local/eComAPI/v1/session/" + sessionId + "/application");
            } else {
                url = new URL("http://vdc-qaswebapp" + environment + ".stormwind.local/eComAPI/v1/session/" + sessionId + "/application");
            }
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Authorization", "Bearer " + getBearer());
            connection.setReadTimeout(8*1000);
            connection.getResponseCode();

            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String line;

            while ((line = reader.readLine()) != null) {
                response.append(line + "\n");
            }

            reader.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return response.toString();
    }

    public static String getLease(String leaseId) {

        StringBuilder response = new StringBuilder();
        URL url;

        try
        {
            if (environment.equals("RC")) {
                url = new URL("https://slc-rcpwebcor.stormwind.local/LeaseAdminPM/LeaseAdmin?LeaseAdministrationAction=GetLease&AccountId=" + leaseId + "&Action=GetModel&IsGracePeriodExcluded=false");
            } else {
                url = new URL("http://vdc-qaswebapp" + environment + ".stormwind.local/LeaseAdminPM/LeaseAdmin?LeaseAdministrationAction=GetLease&AccountId=" + leaseId + "&Action=GetModel&IsGracePeriodExcluded=false");
            }

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setReadTimeout(8*1000);
            connection.getResponseCode();

            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String line;

            while ((line = reader.readLine()) != null)
            {
                response.append(line + "\n");
            }

            reader.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return response.toString();

    }

    public static String getCart(String sessionId) {

        StringBuilder response = new StringBuilder();
        URL url;

        try
        {
            if (environment.equals("RC")) {
                url = new URL("https://slc-rcpwebpws.stormwind.local/eComAPI/v1/session/" + sessionId);
            } else {
                url = new URL("http://vdc-qaswebapp" + environment + ".stormwind.local/eComAPI/v1/session/" + sessionId);
            }

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Authorization", "Basic dGVzdE1lcmNoYW50MTp0ZXN0MTIz");
            connection.setReadTimeout(8*1000);
            connection.getResponseCode();

            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String line;

            while ((line = reader.readLine()) != null)
            {
                response.append(line + "\n");
            }

            reader.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return response.toString();
    }

//    public static void main(String[] args) {
//        System.out.println(getSessionInitial());
//        validateText("\"sessionId\":\""+ sid + "\",\"street1\":\"123 Fake St\",\"city\":\"Draper\",\"state\":\"UT\",\"zip\":\"84020\"", "getPayment");
//        validateText("b73e48ce-d901-4681-a9d8-38271c44c783", "\"orderAmount\":550,\"sku\":\"MACU12312\"", "getCart");
//        validateText("c7ba5982-8345-4f24-b4d5-a837db4d84f0", "\"givenName\":\"Tester\"", "getApplication");
//        validateText("\"AccountId\":" + leaseId + ",\"ApprovalLimit\":1000.0000,\"EffectiveLeaseBalance\":1125.00,\"NinetyDayBuyout\":550.00", "getLease");
//        System.out.println(getCart("b73e48ce-d901-4681-a9d8-38271c44c783"));
//    }
}