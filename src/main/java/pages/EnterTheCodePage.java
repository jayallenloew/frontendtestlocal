/*
 * Copyright Progressive Leasing LLC.
 */
package pages;

import common.FieldValidationUtilities;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import utilities.WebElementGetter;

import java.util.HashSet;
import java.util.Set;

/**
 * Test class for page object for the "Enter the code" page. Re-enter the code you 
 * just rcvd via 2FA. 
 * <p>
 * Path: Store demo landing page | Get started | Enter mobile or email, accept terms, click Get started.
 * <p>
 * The click-through is to this page, which we'll call Enter the code, where 
 * the code rcvd on your phone or whatever must be correctly entered.
 * 
 * Version 1.0 2019-01-08
 * <p>
 * Versiohn 1.1 2019-03-18
 * <p>
 * Version 1.2 2019-06-21 Move landing check into the page object out of downstream test.
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @author <a href="mailto:christopher.rains@progleasing.com">CJ Rains</a>
 * @version 1.2
 * @see EnterTheCodePageTests_A.java
 */
public class EnterTheCodePage {

	private WebDriver driverIn;
	
	public EnterTheCodePage(WebDriver driverIn) { 
		this.driverIn = driverIn;
		try { 
			new WebDriverWait(driverIn, 30).until(ExpectedConditions.presenceOfElementLocated((By.id("pg-field-code"))));
		}catch(TimeoutException tE) { 
			throw new AssertionError("\n\n" + getClass().getSimpleName() + ": double-check page: " + driverIn.getCurrentUrl() + "\n",tE);
		}
	}
	
	
	/**
	 * Fetch an instance of org.openqa.selenium.WebElement representing the Enter the code field.
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getFieldEnterTheCode() { 
		
		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();
		
		Set<By> locators = new HashSet<By>();
		locators.add(By.id("pg-field-code"));
		locators.add(By.cssSelector("#pg-field-code"));
		locators.add(By.xpath("//input[@id='pg-field-code']"));
		
		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}		
	
	/**
	 * Fetch an instance of org.openqa.selenium.WebElement representing the "last 4" field:<br>
	 * Last 4 of SSN / ITIN
	 * <p>
	 * This field is not displayed in each and every happy-path flow. You'll see 
	 * it occasionally, not every time.
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getFieldLast4() { 
		
		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();
		
		Set<By> locators = new HashSet<By>();
		locators.add(By.id("pg-ssn-field"));
		locators.add(By.cssSelector("#pg-ssn-field"));
		locators.add(By.xpath("//input[@id='pg-ssn-field']"));
		
		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}

	
	/**
	 * Fetch an instance of org.openqa.selenium.WebElement representing the Exit button upper right.
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getButtonExit() { 
		
		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();
		
		Set<By> locators = new HashSet<By>();
		locators.add(By.xpath("//*[@id=\"exit-button\"]"));
		locators.add(By.xpath("//button[@id='exit-button'])[2]"));
		locators.add(By.xpath("//div[2]/div[3]/button")); 
		
		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}		
	

	
	/**
	 * Fetch an instance of org.openqa.selenium.WebElement representing the Resent link.
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getLinkResend() { 
		
		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();
		
		Set<By> locators = new HashSet<By>();
		locators.add(By.id("resend-link"));
		locators.add(By.cssSelector("#resend-link"));
		locators.add(By.xpath("//a[contains(text(),'Resend')]"));
		
		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}		
	
	
	public WebElement getElementErrorInvalidCode() { 
		
		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();
		
		Set<By> locators = new HashSet<By>();
		locators.add(By.xpath("//pg-otp-enter-code-field/pg-input-factory/pg-field/div[3]/pg-subscript-messages/span"));
		locators.add(By.cssSelector("pg-field.pg-field.grit.should-label-float.is-error.is-hovered > div.pg-field-subscript-container > pg-subscript-messages.pg-subscript-messages > span.pg-error.pg-subscript-message.ng-star-inserted"));
		
		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}
	
	
	/**
	 * Optional helper. You'll need to manage wait times in your downstream test.
	 * <p>
	 * @param codeToEnter
	 * @throws InterruptedException
	 */
	public void enterThisCode(String codeToEnter, String... last4ToEnter) {
	
		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

		getFieldEnterTheCode().clear();
		getFieldEnterTheCode().sendKeys(codeToEnter);
		FieldValidationUtilities.sleepSecond(methodNameLocal);
		try { 
			if(last4ToEnter.length>0) { 
				getFieldLast4().clear();
				getFieldLast4().sendKeys(last4ToEnter);
				FieldValidationUtilities.sleepSecond(methodNameLocal);
			}
		}catch(NullPointerException nPE) { 
			// deliberate swallow
		}
	}
	
}
