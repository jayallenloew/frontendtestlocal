/*
 * Copyright Progressive Leasing LLC.
 */
package pages;

import java.util.HashSet;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import common.FieldValidationUtilities;
import utilities.WebElementGetter;

/**
 * Test class for page object for the "request code" page; enter mobile or email and accept terms. 
 * <p>
 * Path: Store demo landing page | Get started
 * <p>
 * There are just a few things we're responsible for on this page, so 
 * this page object is small.
 * <p>
 * Version 1.0 2019-01-08
 * <p>
 * Version 1.1 2019-06-21 add check for primary field in the constructor and 
 * remove it from downstream tests.
 * <p>
 * Version 1.2 2019-06-21 Move landing check into the page object out of downstream test.
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @author <a href="mailto:christopher.rains@progleasing.com">CJ Rains</a>
 * @version 1.2
 * @see GetStartedRequestCodePageTests.java
 */
public class GetStartedRequestCodePage {

	private WebDriver driverIn;
		
	public GetStartedRequestCodePage(WebDriver driverIn) { 
		this.driverIn = driverIn;
		try { 
			new WebDriverWait(driverIn, 30).until(ExpectedConditions.presenceOfElementLocated((By.id("pg-field-contact"))));
		}catch(TimeoutException tE) { 
			throw new AssertionError("\n\n" + getClass().getSimpleName() + ": double-check page: " + driverIn.getCurrentUrl() + "\n",tE);
		}
	}
	
	
	/**
	 * Fetch an instance of org.openqa.selenium.WebElement representing Mobile phone or email field.
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getFieldMobileOrEmail() { 
		
		String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
		
		Set<By> locators = new HashSet<By>();
		locators.add(By.id("pg-field-contact"));
		locators.add(By.cssSelector("#pg-field-contact"));
		locators.add(By.xpath("//input[@id='pg-field-contact']"));
		
		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}

	/**
	 * Fetch an instance of org.openqa.selenium.WebElement representing the disclosures checkbox "I have read and agreed..."
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getCheckboxTerms() { 
		
		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();
		
		Set<By> locators = new HashSet<By>();
		locators.add(By.cssSelector("div.check-control"));
		locators.add(By.xpath("//pg-checkbox/div"));
		
		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}	
	

	/**
	 * Fetch an instance of org.openqa.selenium.WebElement representing the Resume button.
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getButtonResume() { 
		
		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();
		
		Set<By> locators = new HashSet<By>();
		locators.add(By.cssSelector("button.pg-button.neutral"));
		locators.add(By.xpath("//app-welcome/div/div[3]/button"));
		locators.add(By.xpath("//button[@type='neutral']"));
		
		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}	

	

	/**
	 * Fetch an instance of org.openqa.selenium.WebElement representing the Get started button (send the code).
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getButtonContinue() { 
		
		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();
		
		Set<By> locators = new HashSet<By>();
		locators.add(By.id("btn-send-code"));
		locators.add(By.id("continue-button"));
		locators.add(By.cssSelector("#continue-button"));
		locators.add(By.xpath("//button[@id='continue-button']"));
		
		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}	


	

	/**
	 * Fetch an instance of org.openqa.selenium.WebElement representing the Exit button upper right.
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getButtonExit() { 
		
		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();
		
		Set<By> locators = new HashSet<By>();
		locators.add(By.xpath("//pg-header[@id='main-header']/div[2]/div[3]/button"));
		locators.add(By.xpath("//div[2]/div[3]/button"));
		
		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}	
	
	
	
	/**
	 * Happy-path helper method for requesting the code. 
	 * <p>
	 * @param mobileOrEmail
	 */
	public void happyPathRequestCode(String mobileOrEmail) {
		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();
		FieldValidationUtilities.sleepSecond(methodNameLocal); // don't remove; Chrome needs it
		getFieldMobileOrEmail().sendKeys(mobileOrEmail);
		FieldValidationUtilities.sleepSecond(methodNameLocal);
		try { 
			getCheckboxTerms().click();
		}catch(Exception anyExceptionWhatsoever) { 
			throw new IllegalStateException(methodNameLocal, anyExceptionWhatsoever);
		}
		FieldValidationUtilities.sleepSecond(methodNameLocal);
		
//		Actions builder = new Actions(driverIn);
//		builder.sendKeys(Keys.TAB).build().perform();
//		FieldValidationUtilities.sleepSecond(methodNameLocal);
//		builder.sendKeys(Keys.TAB).build().perform();
//		FieldValidationUtilities.sleepSecond(methodNameLocal);
//		builder.sendKeys(Keys.TAB).build().perform();
//		FieldValidationUtilities.sleepSecond(methodNameLocal);
//		builder.sendKeys(Keys.TAB).build().perform();
//		FieldValidationUtilities.sleepSecond(methodNameLocal);
//		builder.sendKeys(Keys.TAB).build().perform();
//		FieldValidationUtilities.sleepSecond(methodNameLocal);
//		builder.sendKeys(Keys.SPACE).build().perform();
//		FieldValidationUtilities.sleepSecond(methodNameLocal);
//		FieldValidationUtilities.sleepSecond(methodNameLocal);
//		FieldValidationUtilities.sleepSecond(methodNameLocal);
		
		getButtonContinue().click();
		FieldValidationUtilities.sleepSecond(methodNameLocal);
		FieldValidationUtilities.sleepSecond(methodNameLocal);
		FieldValidationUtilities.sleepSecond(methodNameLocal);
	}

}
