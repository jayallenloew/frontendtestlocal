/*
 * Copyright Progressive Leasing LLC.
 */
package pages;

import java.util.HashSet;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import common.FieldValidationUtilities;
import utilities.WebElementGetter;

/**
 * Test class for page object for the upcoming "short flow" basic info page. 
 * <p>
 * Field reduction.
 * <p>
 * Remaining: first name field, last name field, DOB field, and Continue to contact info button.
 * <p>
 * Class name is temporary. After they turn this on in production, we'll archive-remove 
 * BasicInfoPage.java and then rename this. 
 * <p>
 * Path to this page: 
 * <p>
 * Marketing page --> Get started | Request code (complete & submit) | Enter code
 * <p>
 * Version 1.0 2019-06-03
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @author <a href="mailto:christopher.rains@progleasing.com">CJ Rains</a>
 * @see RequestCodePageTests.java
 */
public class BasicInfoPageShortFlow {

	private WebDriver driverIn;

	public BasicInfoPageShortFlow(WebDriver driverIn) { 
		this.driverIn = driverIn;
	}

	/**
	 * Expected error message when an illegal month (such as 33) is entered.
	 */
	public static final String msgErrorExpectedMonth = 
			"The month you entered is invalid.";


	/**
	 * Expected error message when an illegal year (such as 199 or 19 [too short]) is entered.
	 */
	public static final String msgErrorExpectedYear = 
			"The year you entered is invalid.";


	/**
	 * Expected error message when an invalid day is entered.
	 * <p>
	 * This same error message is expected when:
	 * <ul>
	 * <li>Day is a mis-match for the month, i.e. 31 September.</li>
	 * <li>Day is illegal, i.e. 00 or 99.</li>
	 * </ul>
	 */
	public static final String msgErrorExpectedDay = 
			"The day you added is invalid for the entered month.";

	/**
	 * Expected error message when date of birth is skipped altogether.
	 */
	public static final String msgErrorExpectedDOBNull = 
			"Your date of birth is required to process the application.";

	/**
	 * Expected error message when applicant is legally underage.
	 */	
	public static final String msgErrorExpectedUnderage = 
			"You must be at least 18 years old to apply for a lease.";
	
	/**
	 * Expected error message when applicant is legally overage.
	 */	
	public static final String msgErrorExpectedOverage = 
			"You must be under 100 years old to apply for a lease.";

	/**
	 * Fetch a WebElement representing the Exit button, upper right.
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getButtonExit() { 

		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

		Set<By> locators = new HashSet<By>();
		locators.add(By.xpath("//button[@id='exit-button'])[2]"));
		locators.add(By.cssSelector(".ng-star-inserted > #exit-button"));
		locators.add(By.xpath("//div[2]/div[3]/button"));

		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}		


	/**
	 * Fetch a WebElement representing the First name field.
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getFieldNameFirst() { 

		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

		Set<By> locators = new HashSet<>();
		locators.add(By.id("first-name-input"));
		locators.add(By.cssSelector("#first-name-input"));
		locators.add(By.xpath("//input[@id='first-name-input']"));

		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}		


	/**
	 * Fetch a WebElement representing the First name field.
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getFieldNameLast() { 

		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

		Set<By> locators = new HashSet<By>();
		locators.add(By.id("last-name-input"));
		locators.add(By.cssSelector("#last-name-input"));
		locators.add(By.xpath("//input[@id='last-name-input']"));

		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}		


	/**
	 * Fetch a WebElement representing the two-character month portion of the Date of birth.
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getFieldDate_MM() { 

		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

		Set<By> locators = new HashSet<By>();
		locators.add(By.id("dob-month-input"));
		locators.add(By.cssSelector("#dob-month-input"));
		locators.add(By.xpath("//input[@id='dob-month-input']"));

		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}


	/**
	 * Fetch a WebElement representing the two-character day portion of the Date of birth.
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getFieldDate_DD() { 

		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

		Set<By> locators = new HashSet<By>();
		locators.add(By.id("dob-day-input"));
		locators.add(By.cssSelector("#dob-day-input"));
		locators.add(By.xpath("//input[@id='dob-day-input']"));

		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}


	/**
	 * Fetch a WebElement representing the four-character year portion of the Date of birth.
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */	
	public WebElement getFieldDate_YYYY() { 

		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

		Set<By> locators = new HashSet<By>();
		locators.add(By.id("dob-year-input"));
		locators.add(By.cssSelector("#dob-year-input"));
		locators.add(By.xpath("//input[@id='dob-year-input']"));

		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}


	/**
	 * Fetch a WebElement representing the Continue to contact info button.
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getButtonContinue() { 

		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

		Set<By> locators = new HashSet<By>();
		locators.add(By.cssSelector(".positive"));
		locators.add(By.xpath("//button[contains(.,'Continue to contact info')]"));
		locators.add(By.xpath("//pg-app-form-footer/div/button"));

		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}




	/**
	 * Fetch WebElement instance representing the green check mark that appears 
	 * in the Date of birth field during happy-path scenario.
	 * <p>
	 * The check mark appears after a legal value is entered and after click-out 
	 * or tab-out.
	 * <p>
	 * Example: enter 8 digits such as 08121991 for August 12, 1991.
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getDOBHappyCheckbox() { 

		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

		Set<By> locators = new HashSet<By>();
		locators.add(By.cssSelector(".success-icon"));
		locators.add(By.xpath("//i[contains(.,'done')]"));

		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}





	/**
	 * Fetch WebElement instance representing the message displayed below the 
	 * date of birth field during negative scenario null (skip date entirely).
	 * <p>
	 * "Your date of birth is required to process the application."
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getDOBPrimaryErrorMessage() { 
		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

		Set<By> locators = new HashSet<By>();
		locators.add(By.cssSelector(".is-error .pg-field-error:nth-child(1)"));
		locators.add(By.xpath("//span[contains(.,'Your date of birth is required to process the application.')]"));
		locators.add(By.xpath("//pg-field-wrapper/div[2]/span"));

		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}


	/**
	 * Fetch WebElement instance representing the message displayed below the 
	 * date of birth field during negative scenarios involving invalid day.
	 * <p>
	 * "The day you added is invalid for the entered month."
	 * <p>
	 * Examples: 31 September, or 30 February.
	 * <p>
	 * Also expected in response to illegal days, i.e. 00 or 99.
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getDOBErrorMessageDD() { 
		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

		Set<By> locators = new HashSet<By>();
		locators.add(By.cssSelector(".is-error .pg-field-error:nth-child(3)"));
		locators.add(By.xpath("//span[contains(.,'The day you added is invalid for the entered month.')]"));

		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}


	/**
	 * Fetch WebElement instance representing the message displayed below the 
	 * date of birth field during negative scenario involving invalid month.
	 * <p>
	 * "The month you entered is invalid."
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getDOBErrorMessageMM() { 
		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

		Set<By> locators = new HashSet<By>();
		locators.add(By.cssSelector(".is-error .pg-field-error:nth-child(2)"));
		locators.add(By.xpath("//span[contains(.,'The month you entered is invalid.')]"));


		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}


	/**
	 * Fetch WebElement instance representing the message displayed below the 
	 * date of birth field during negative scenario involving invalid year.
	 * <p>
	 * "The year you entered is invalid."
	 * <p>
	 * Expected when: digits < 4 or YYYY null.
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getDOBErrorMessageYYYY() { 
		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

		Set<By> locators = new HashSet<By>();
		locators.add(By.cssSelector(".pg-field-error:nth-child(6)"));
		locators.add(By.xpath("//pg-date-of-birth[@id='dob']/div/pg-field-wrapper/div[2]/span[6]"));
		locators.add(By.xpath("//span[6]"));

		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}


	/**
	 * Fetch WebElement instance representing the message displayed below the 
	 * date of birth field during negative scenario involving an applicant who 
	 * is more than 100 years old (disallowed).
	 * <p>
	 * "You must be under 100 ..."
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getDOBErrorMessageYYYYCenturyOld() { 
		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

		Set<By> locators = new HashSet<By>();
		locators.add(By.cssSelector(".pg-field-error:nth-child(5)"));
		locators.add(By.xpath("//pg-date-of-birth[@id='dob']/div/pg-field-wrapper/div[2]/span[5]"));
		locators.add(By.xpath("//span[5]"));

		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}


	/**
	 * Fetch WebElement instance representing the message displayed below the 
	 * date of birth field during negative scenario involving an applicant who 
	 * is <= 18 years old (disallowed).
	 * <p>
	 * "You must be at least 100 ..."
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getDOBErrorMessageYYYYUnderage() { 
		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

		Set<By> locators = new HashSet<By>();
		locators.add(By.cssSelector(".pg-field-error:nth-child(4)"));
		locators.add(By.xpath("//pg-date-of-birth[@id='dob']/div/pg-field-wrapper/div[2]/span[4]"));
		locators.add(By.xpath("//span[4]"));

		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}


	/**
	 * Fetch WebElement instance representing the Issuing State field.
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getFieldIssuingState() { 
		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

		Set<By> locators = new HashSet<By>();
		locators.add(By.id("issuing-state-input"));
		locators.add(By.cssSelector("#issuing-state-input"));
		locators.add(By.xpath("//input[@id='issuing-state-input']"));

		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}


	/**
	 * Optional helper method: enter a full name.
	 * <p>
	 * @param first
	 * @param last
	 */
	public void enterName(String first, String last) {

		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

		getFieldNameFirst().clear();
		getFieldNameFirst().sendKeys(first);
		FieldValidationUtilities.sleepQuarterSecond(methodNameLocal);

		getFieldNameLast().clear();
		getFieldNameLast().sendKeys(last);
		FieldValidationUtilities.sleepQuarterSecond(methodNameLocal);
	}




	/**
	 * Optional helper method: enter a legal date of birth.
	 * <p>
	 * @param twoDigitMonth
	 * @param twoDigitDay
	 * @param fourDigitYear
	 */
	public void enterDate(String twoDigitMonth, String twoDigitDay, String fourDigitYear) {

		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

		WebElement month = getFieldDate_MM();
		month.clear();
		month.sendKeys(twoDigitMonth);
		FieldValidationUtilities.sleepQuarterSecond(methodNameLocal);

		WebElement day = getFieldDate_DD();
		day.clear();
		day.sendKeys(twoDigitDay);
		FieldValidationUtilities.sleepQuarterSecond(methodNameLocal);

		WebElement year = getFieldDate_YYYY();
		year.clear();
		year.sendKeys(fourDigitYear);
		FieldValidationUtilities.sleepQuarterSecond(methodNameLocal);
	}




}
