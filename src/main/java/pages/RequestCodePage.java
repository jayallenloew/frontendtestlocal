/*
 * Copyright Progressive Leasing LLC.
 */
package pages;

import java.util.HashSet;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import utilities.WebElementGetter;
import waiter.Waiter;

/**
 * Test class for page object for the "request code" page; enter mobile or email and accept terms. 
 * <p>
 * Path: Store demo landing page | Get started
 * <p>
 * There are just a few things we're responsible for on this page, so 
 * this page object is small.
 * <p>
 * Version 1.0 2019-01-08
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @author <a href="mailto:christopher.rains@progleasing.com">CJ Rains</a>
 * @see RequestCodePageTests.java
 */
public class RequestCodePage {

	private WebDriver driverIn;
	
	private Waiter waiter;
	
	public RequestCodePage(WebDriver driverIn) { 
		this.driverIn = driverIn;
		waiter = new Waiter();
	}
	
	/**
	 * Fetch an instance of org.openqa.selenium.WebElement representing Mobile phone or email field.
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getFieldMobileOrEmail() { 
		
		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();
		
		Set<By> locators = new HashSet<By>();
		locators.add(By.id("otp-contact-input"));
		locators.add(By.cssSelector("#otp-contact-input"));
		locators.add(By.xpath("//input[@id='otp-contact-input']"));
		
		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}

	/**
	 * Fetch an instance of org.openqa.selenium.WebElement representing the disclosures checkbox "I have read and agreed..."
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getCheckboxTerms() { 
		
		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();
		
		Set<By> locators = new HashSet<By>();
		locators.add(By.cssSelector("span.pg-checkbox-mark.primary"));
		locators.add(By.xpath("//pg-checkbox[@id='agree-to-terms']/div/label/span"));
		
		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}	
	

	/**
	 * Fetch an instance of org.openqa.selenium.WebElement representing the Resume button.
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getButtonResume() { 
		
		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();
		
		Set<By> locators = new HashSet<By>();
		locators.add(By.cssSelector("button.pg-button.neutral"));
		locators.add(By.xpath("//app-welcome/div/div[3]/button"));
		locators.add(By.xpath("//button[@type='neutral']"));
		
		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}	

	

	/**
	 * Fetch an instance of org.openqa.selenium.WebElement representing the Get started button (send the code).
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getButtonSendCode_Get_started() { 
		
		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();
		
		Set<By> locators = new HashSet<By>();
		locators.add(By.id("btn-send-code"));
		locators.add(By.cssSelector("#btn-send-code"));
		locators.add(By.xpath("//button[@id='btn-send-code']"));
		
		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}	


	

	/**
	 * Fetch an instance of org.openqa.selenium.WebElement representing the Exit button upper right.
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getButtonExit() { 
		
		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();
		
		Set<By> locators = new HashSet<By>();
		locators.add(By.xpath("//pg-header[@id='main-header']/div[2]/div[3]/button"));
		locators.add(By.xpath("//div[2]/div[3]/button"));
		
		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}	
	
	
	
	/**
	 * Happy-path helper method for requesting the code. 
	 * <p>
	 * Add The Waiter methods and re-test. Better performance and code reduction.
	 * @param mobileOrEmail
	 */
	public void happyPathRequestCode(String mobileOrEmail) {
		WebElement fieldTemp = getFieldMobileOrEmail();
		try { 
			Thread.sleep(500);
		}catch(InterruptedException iE) { 
			// deliberate swallow
		}
		fieldTemp.clear();
		try { 
			Thread.sleep(500);
		}catch(InterruptedException iE) { 
			// deliberate swallow
		}
		fieldTemp.sendKeys(mobileOrEmail);
		try { 
			Thread.sleep(1000);
		}catch(InterruptedException iE) { 
			// deliberate swallow
		}
		waiter.click(getCheckboxTerms(), driverIn, 60);
		try { 
			Thread.sleep(1000);
		}catch(InterruptedException iE) { 
			// deliberate swallow
		}
		waiter.click(getButtonSendCode_Get_started(), driverIn, 60);
		try { 
			Thread.sleep(1000);
		}catch(InterruptedException iE) { 
			// deliberate swallow
		}
	}

}
