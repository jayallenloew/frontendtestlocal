package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import utilities.WebElementGetter;

import java.util.HashSet;
import java.util.Set;

/**
 * Path to this page:
 * <p>
 * Marketing page -> Get started | Request code (complete & submit) | Enter code | BasicInfo page | ContactInfo page | BankInfo page -> Apply
 * <p>
 * Version 1.0 2019-06-17
 * <p>
 * @author <a href="mailto:jay.loew@progleasing.com">Allen Loew</a>
 * @author <a href="mailto:christopher.rains@progleasing.com">CJ Rains</a>
 */
public class DisclosurePage {

    private WebDriver driverIn;

    public DisclosurePage(WebDriver driverIn) {
        this.driverIn = driverIn;
    }


    /**
     * Fetch a WebElement representing the Apply Now button.
     * <p>
     * @return An instance of org.openqa.selenium.WebElement
     */
    public WebElement getButtonApply() {

        String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

        Set<By> locators = new HashSet<By>();
        locators.add(By.xpath("/html/body/app-root/app-application-form/pg-template-layout/pg-app-form-template/div[3]/pg-app-form-footer/div/button"));
        locators.add(By.cssSelector("body > app-root > app-application-form > pg-template-layout > pg-app-form-template > div.pg-app-form-footer-wrapper > pg-app-form-footer > div > button"));

        return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
    }
}
