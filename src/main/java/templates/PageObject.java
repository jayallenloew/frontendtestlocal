package templates;

import java.util.HashSet;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import utilities.WebElementGetter;

/**
 * A properly designed page object as an example, using the Luma storefront landing page.
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @author <a href="mailto:christopher.rains@progleasing.com">CJ Rains</a>
 * @version 1.0 2019-01-04
 */
public class PageObject {
	
	private WebDriver driverIn;
	
	public PageObject(WebDriver driverIn) { 
		this.driverIn = driverIn;
	}
	
	/**
	 * Fetch an instance of org.openqa.selenium.WebElement representing the LEARN MORE 
	 * button on the demo store landing page.
	 * <p>
	 * This supports a typical clickstream for the end user: land on the store page, see 
	 * the Progressive Leasing carousel content, click the LEARN MORE button.
	 * 
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getButtonLEARN_MORE() { 
		
		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();
		
		Set<By> locators = new HashSet<By>();
		locators.add(By.cssSelector("#prog-valprop-5d1147d66f493 > img"));
		locators.add(By.xpath("//img[@alt='Learn more about Progressive Leasing']"));
		
		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}

}
