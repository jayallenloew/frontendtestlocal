/*
 * Copyright Progressive Leasing LLC.
 */
package templates;

import java.lang.reflect.Method;
import java.util.logging.Logger;

import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import common.ProgressiveLeasingConstants;
import utilities.CommonDriversLocal;
import utilities.DriverTypeLocal;

/**
 * Example: Simple front end tests with Selenium. Local test execution.
 * <p>
 * Included features include but not limited to the following: 
 * properly handled assertions, safe waits, annotations, logging.
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @version 1.0
 */
public class LocalTests {
	
	private static final Logger LOGGER = Logger.getLogger(LocalTests.class.getName());

	public static final String SOURCE_SNIPPET_EXPECTED = 
			"http://storedemo.progressivelp.com/pub/";

	@Test(enabled=true,priority=1)
	public void localTestLandingPage(Method method) { 
		
		WebDriver driverLocal = new CommonDriversLocal(DriverTypeLocal.CHROME).getPreparedDriver();
		
		driverLocal.navigate().to(ProgressiveLeasingConstants.URL_STORE_DEMO);
		// Capture source.
		StringBuilder sBuild = new StringBuilder(driverLocal.getPageSource());
		// Assert source contains expected.
		try { 
			Assert.assertTrue(sBuild.toString().contains(LocalTests.SOURCE_SNIPPET_EXPECTED));
			LOGGER.info(method.getName() + ":\t" + "PASS");
		}catch(AssertionError aE) { 
			LOGGER.info(method.getName() + ":\t" + "FAIL");
			throw aE;
		}finally {
			// Clean up whether pass or fail.
			sBuild.delete(0, sBuild.length());
			sBuild = null;
			driverLocal.quit();
		}
	}
	
	@Test(enabled=true,priority=2) 
	public void localTestDemoStoreLandingPage(Method method) { 
		
		WebDriver driverLocal = new CommonDriversLocal(DriverTypeLocal.CHROME).getPreparedDriver();
		
		driverLocal.navigate().to(ProgressiveLeasingConstants.URL_STORE_DEMO);
		
		PageObject pageUnderTest = new PageObject(driverLocal);
		
		pageUnderTest.getButtonLEARN_MORE().click();
		
		try { 
			new WebDriverWait(driverLocal, 5).until(ExpectedConditions.titleIs("Progressive Leasing"));
			LOGGER.info(method.getName() + ":\tPASS");
		}catch(TimeoutException tE) {
			LOGGER.info(method.getName() + ":\t" + "FAIL");
			throw new AssertionError(method.getName(), tE);
		}finally { 
			driverLocal.quit();
		}
	}
}
