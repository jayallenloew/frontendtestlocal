/*
 * Copyright Progressive Leasing LLC.
 */
package flows;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import common.FieldValidationUtilities;
import common.ProgressiveLeasingConstants;
import pages.BankInfoPage;
import pages.BasicInfoPage;
import pages.ContactInfoPage;
import pages.DisclosurePage;
import pages.EnterTheCodePage;
import pages.GetStartedRequestCodePage;
import pages.MarketingPage;
import utilities.ResetThisUser;
import utilities.ServiceTests;
import utilities.TestBaseLocal;


/**
 * This test begins at eCom Welcome page and ends at lease approval.
 * orderAmount is within $150 of approvalAmount.
 * Current default QAEnv approvalAmt = $1000.
 * <p>
 * Version 1.0 2019-06-18
 * <p>
 * @author <a href="mailto:Shawn.Hartke@progleasing.com">Shawn</a>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen</a>
 * @author <a href="mailto:christopher.rains@progleasing.com">CJ</a>
 * @version 1.0
 */
public class G_3540_ResultType0 extends TestBaseLocal {

	private WebDriver driver;

	/**
	 * Test requires any one of these known valid test accounts; pick one at random.
	 */
	public static final String EMAIL_FOR_THIS_TEST = 
			ProgressiveLeasingConstants.TEST_DOT_COM_ADDRESSES[new java.util.Random().nextInt(ProgressiveLeasingConstants.TEST_DOT_COM_ADDRESSES.length)];

	@BeforeMethod
	public void setupNotStatic() {

		// reset the user to be used
		@SuppressWarnings("unused")
		ResetThisUser resetTestAccount = new ResetThisUser(EMAIL_FOR_THIS_TEST);
		resetTestAccount = null;
	}

	@Test
	public void testResultType0() {

		String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();

		//		if Sauce
		//		optional: override whatever the default driver type is currently set to in TestBaseSauce.java
//		setDriverTypeForTestClass(DriverTypeSauce.WINDOWS_FIREFOX);

		//		if Local
		//		optional: override whatever the default driver type is currently set to in TestBaseLocal.java
		//        setDriverTypeForTestClass(DriverTypeLocal.CHROME);

		// generate a new merchant session
		final String sessionId = ServiceTests.getSessionInitial();

		// link an orderInfo record to this merchant session before starting application - $900 for ResultType0
		ServiceTests.putOrder(sessionId, 900);

		// instantiate the driver
		driver = getPreparedDriver();


		// go to eCom Landing page
		driver.navigate().to(ProgressiveLeasingConstants.URL_WELCOME_13 + "?sid="
				+ sessionId
				+ "&returnShoppingRedirect=%2F&successRedirect=https:%2F%2Fgoogle.com");


		//region MarketingPage

		// create MarketingPage object and wait for Get Started button
		MarketingPage marketingTemp = new MarketingPage(driver);

		try {
			new WebDriverWait(driver, 8).until(ExpectedConditions.elementToBeClickable(marketingTemp.getButtonGet_started()));
			System.out.println(methodNameLocal + ":\tmarketing page... (OK)");
		}catch(TimeoutException tE) {
			throw new AssertionError((methodNameLocal + ":\tfast-fail in setup; check marketing page."), tE);
		}

		marketingTemp.getButtonGet_started().click();
		//endregion

		FieldValidationUtilities.sleepSecond(methodNameLocal);

		//region OtpPage

		// create OTP Page object and wait for Mobile phone or email field
		GetStartedRequestCodePage requestCodeTemp = new GetStartedRequestCodePage(driver);

		try {
			new WebDriverWait(driver, 8).until(ExpectedConditions.presenceOfElementLocated(By.id("pg-field-contact")));
			System.out.println(methodNameLocal + ":\tget started request code page... (OK)");
		}catch(TimeoutException tE) {
			throw new AssertionError((methodNameLocal + ":\tfast-fail in setup; check get started request code page."), tE);
		}finally {
			marketingTemp = null;
		}

		// enter a test email address or phone
		requestCodeTemp.getFieldMobileOrEmail().sendKeys(EMAIL_FOR_THIS_TEST);
		FieldValidationUtilities.sleepQuarterSecond(methodNameLocal);

		// accept the terms in order to proceed to enter the code
		requestCodeTemp.getCheckboxTerms().click();
		FieldValidationUtilities.sleepQuarterSecond(methodNameLocal);

		// The continue button here is pretty sketchy so we'll give it two ways
		try {
			requestCodeTemp.getButtonContinue().click();
			FieldValidationUtilities.sleepSecond(methodNameLocal);
		}catch(Exception anyException) {
			// deliberate swallow; messaging only
			System.out.println(methodNameLocal + ":\t" + "failed at get started button; 2nd try up next...");
			System.out.println(methodNameLocal + ":\t" + "exception caught:" + anyException.getClass().getSimpleName());
			// after the checkbox, 5 tabs and a space to advance to the next page
			Actions builder = new Actions(driver);
			builder.sendKeys(Keys.TAB).build().perform();
			FieldValidationUtilities.sleepQuarterSecond(methodNameLocal);
			builder.sendKeys(Keys.TAB).build().perform();
			FieldValidationUtilities.sleepQuarterSecond(methodNameLocal);
			builder.sendKeys(Keys.TAB).build().perform();
			FieldValidationUtilities.sleepQuarterSecond(methodNameLocal);
			builder.sendKeys(Keys.TAB).build().perform();
			FieldValidationUtilities.sleepQuarterSecond(methodNameLocal);
			builder.sendKeys(Keys.TAB).build().perform();
			FieldValidationUtilities.sleepQuarterSecond(methodNameLocal);
			builder.sendKeys(Keys.SPACE).build().perform();
			FieldValidationUtilities.sleepQuarterSecond(methodNameLocal);
			builder = null;
		}finally {
			requestCodeTemp = null;
		}
		//endregion

		FieldValidationUtilities.sleepSecond(methodNameLocal);

		//region EnterTheCodePage

		//create CodePage object and wait for 'enter the code' field
		EnterTheCodePage codePageTemp = new EnterTheCodePage(driver);

		try {
			new WebDriverWait(driver, 8).until(ExpectedConditions.presenceOfElementLocated(By.id("pg-field-code")));
			System.out.println(methodNameLocal + ":\tenter the code page... (OK)");
		}catch(TimeoutException tE) {
			throw new AssertionError((methodNameLocal + ":\tfast-fail in setup; check enter the code page."), tE);
		}

		// enter the (test) code
		codePageTemp.enterThisCode("000000");
		System.out.println(methodNameLocal + ":\tcode sent... (OK)");
		codePageTemp = null;
		//endregion

		// sleep required
		try {
			Thread.sleep(8000); // Don't remove or reduce. It's not our issue.
		}catch(InterruptedException iE) {
			throw new AssertionError((methodNameLocal + ":\tfast-fail in setup"), iE);
		}

		//region BasicInfoPage

		// wait for basic info page
		BasicInfoPage basicTemp = new BasicInfoPage(driver);

		try {
			new WebDriverWait(driver, 8).until(ExpectedConditions.elementToBeClickable(basicTemp.getFieldNameFirst()));
			System.out.println(methodNameLocal + ":\tcode received; at basic info page... (OK)");
		}catch(TimeoutException tE) {
			throw new AssertionError((methodNameLocal + ":\tfast-fail in setup; check basic info page."), tE);
		}

		// name first & last
		basicTemp.enterName(ProgressiveLeasingConstants.BASIC_INFO_555121212[0],
				ProgressiveLeasingConstants.BASIC_INFO_555121212[1]);

		// date MM DD YYYY
		basicTemp.enterDate(ProgressiveLeasingConstants.BASIC_INFO_555121212[2],
				ProgressiveLeasingConstants.BASIC_INFO_555121212[3],
				ProgressiveLeasingConstants.BASIC_INFO_555121212[4]);

		// SSN ### ### ####
		basicTemp.enterSSN(ProgressiveLeasingConstants.BASIC_INFO_555121212[5],
				ProgressiveLeasingConstants.BASIC_INFO_555121212[6],
				ProgressiveLeasingConstants.BASIC_INFO_555121212[7]);

		// driver license or government ID; in QA it's typically a 5-digit integer
		basicTemp.enterDriverLicense(ProgressiveLeasingConstants.BASIC_INFO_555121212[8]);

		// two-letter state or N/A; in QA it's typically UT
		basicTemp.getFieldIssuingState().sendKeys(ProgressiveLeasingConstants.BASIC_INFO_555121212[9]);

		basicTemp.getButtonContinue().click();
		basicTemp = null;
		//endregion

		FieldValidationUtilities.sleepSecond(methodNameLocal);

		//region ContactInfoPage

		// wait for contact info page
		ContactInfoPage contactTemp = new ContactInfoPage(driver);

		try {
			new WebDriverWait(driver, 8).until(ExpectedConditions.presenceOfElementLocated(By.id("mobile-phone-area-code-input")));
			System.out.println(methodNameLocal + ":\tcontact info page... (OK)");
		}catch(TimeoutException tE) {
			throw new AssertionError((methodNameLocal + ":\tfast-fail in setup; check contact info page."), tE);
		}

		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,300)", "");
		jse = null;

		String[] validUT = {
				EMAIL_FOR_THIS_TEST,
				ProgressiveLeasingConstants.CONTACT_INFO_84095[0],	// mobile phone
				ProgressiveLeasingConstants.CONTACT_INFO_84095[1],	// address line 1
				ProgressiveLeasingConstants.CONTACT_INFO_84095[2],	// address line 2 (default null) 												// address line 2
				ProgressiveLeasingConstants.CONTACT_INFO_84095[3],	// city
				ProgressiveLeasingConstants.CONTACT_INFO_84095[4],	// state
				ProgressiveLeasingConstants.CONTACT_INFO_84095[5]	// zip
		};

		contactTemp.happyPathPopulate(validUT);
		System.out.println(methodNameLocal + ": happy path populate contact info... (OK)");

		FieldValidationUtilities.sleepSecond(methodNameLocal);

		contactTemp.getButtonContinue_to_bank_info().click();
		contactTemp = null;
		validUT = null;
		//endregion

		FieldValidationUtilities.sleepSecond(methodNameLocal);

		//region BankInfoPage

		// wait for banking info page
		BankInfoPage bankTemp = new BankInfoPage(driver);

		try {
			new WebDriverWait(driver, 8).until(ExpectedConditions.elementToBeClickable(bankTemp.getFieldRoutingNumber()));
			System.out.println(methodNameLocal + ": banking info page... (OK)");
		}catch(TimeoutException tE) {
			throw new AssertionError((methodNameLocal + ":\tfast-fail in setup; check banking info page."), tE);
		}

		bankTemp.happyPathPopulate();
		System.out.println(methodNameLocal + ": banking info happy path populate complete... (OK)");

		bankTemp.getButtonContinue_to_apply().click();
		//endregion

		FieldValidationUtilities.sleepSecond(methodNameLocal);

		//region DisclosurePage

		// wait for Apply Now button
		DisclosurePage disclosurePage = new DisclosurePage(driver);

		jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,800)", "");
		jse = null;

		try {
			new WebDriverWait(driver, 20).until(ExpectedConditions.elementToBeClickable(disclosurePage.getButtonApply()));
			System.out.println(methodNameLocal + ": disclosure page... (OK)");
		}catch(TimeoutException tE) {
			throw new AssertionError((methodNameLocal + ":\tfast-fail in setup; check disclosure page."), tE);
		}

		disclosurePage.getButtonApply().click();
		//endregion

		FieldValidationUtilities.sleepSecond(methodNameLocal);

		//region ResultPage

		WebElement leaseIDTemp = null;

		try {
			new WebDriverWait(driver, 20).until(ExpectedConditions.presenceOfElementLocated(By.id("applicant-lease-id")));
			System.out.println(methodNameLocal + ": A Lease ID element was detected on 1st try.... (OK)");
		}catch(TimeoutException tE) {
			// deliberate swallow
			System.out.println(methodNameLocal + ": one more try for Lease ID element.... (OK)");
		}

		FieldValidationUtilities.sleepSecond(methodNameLocal);
		FieldValidationUtilities.sleepSecond(methodNameLocal);

		try {
			leaseIDTemp = driver.findElement(By.id("applicant-lease-id"));
		}catch(Exception anyExceptionWhatsoever) {
			// deliberate swallow there will be another try
		}

		if(null==leaseIDTemp) {
			try {
				new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//app-results/div/div/div[3]")));
				System.out.println(methodNameLocal + ": A Lease ID element was detected on 2nd try.... (OK)");
			}catch(TimeoutException tE) {
				throw new AssertionError(methodNameLocal + ": second try FAILED; test will fail", tE);
			}

			FieldValidationUtilities.sleepSecond(methodNameLocal);
			FieldValidationUtilities.sleepSecond(methodNameLocal);

			try {
				leaseIDTemp = driver.findElement(By.xpath("//app-results/div/div/div[3]"));
			}catch(Exception anyExceptionWhatsoever) {
				throw new AssertionError(methodNameLocal + ": second try FAILED; test will fail", anyExceptionWhatsoever);
			}
		}

		try {
			Assert.assertTrue(leaseIDTemp.getText().contains("Lease ID"));
			System.out.println(methodNameLocal + ": captured \'" + leaseIDTemp.getText() + "\'... (good)");
		}catch (AssertionError aE) {
			System.out.println(methodNameLocal + ": FAIL; expected a Lease ID value here.");
			throw aE;
		}finally {
			leaseIDTemp = null;
		}


		
		/*
		 * Begin check for zero dollar approval amount.
		 */
		
		WebElement approvalAmount = null;
		try { 
			approvalAmount = driver.findElement(By.id("applicant-approval-amount"));
		}catch(Exception any) { 
			System.out.println("\n" + methodNameLocal + ": fast-fail: we need but can't capture approval amount" + "\n");
			throw new AssertionError(methodNameLocal + ":\t" + any.getClass().getSimpleName(), any);
		}

		String approvalAmountCaptured = null;
		try { 
			/*
			 * Unlike Denied, which is a valid possibility, 
			 * an approval with a $0 amount is never valid. 
			 * We must immediately fail any test with a $0 
			 * approval amount.
			 */
			approvalAmountCaptured = approvalAmount.getText();
			Assert.assertFalse(approvalAmountCaptured.equals("$0"));
			System.out.println(methodNameLocal + ": captured \'" + approvalAmountCaptured + "\'... (good)");
		}catch(AssertionError aE) { 
			System.out.println("\n" + methodNameLocal + ": fast-fail: approval amount captured: " + approvalAmountCaptured + "\n");
			throw aE;
		}finally { 
			approvalAmountCaptured = null;
		}
		
		/*
		 * End check for zero dollar approval amount.
		 */
		

		
		try {
			new WebDriverWait(driver, 8).until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("/html/body/app-root/app-application-form/pg-template-layout/pg-app-form-template/div[2]/div[1]/pg-notification/div/div/div[2]/div[2]/div[2]/pg-notification-action")));
			System.out.println(methodNameLocal + ": PASS - final");
		}catch(TimeoutException tE){
			System.out.println(methodNameLocal + ": FAIL; expected no notification here.");
			throw tE;
		}
		//endregion
	}


	/**
	 * If subclass of TestBaseSauce, pass the test result up to the Sauce Labs Dashboard.
	 * <p>
	 * Quit driver.
	 */
	@AfterMethod
	public void tearDown(ITestResult result) throws Exception {
		String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
		if(this.getClass().getSuperclass().getSimpleName().equals("TestBaseSauce")) {
			System.out.println(methodNameLocal + ":\tSauce driver type detected; Sauce-specific teardown up next...");
			((JavascriptExecutor) driver).executeScript("sauce:job-result=" + (result.isSuccess() ? "passed" : "failed"));
		} else {
			System.out.println(methodNameLocal + ":\tLocal driver type detected; Sauce-specific teardown skipped intentionally.\n");
		}
		driver.quit();
		System.out.println("\n" + methodNameLocal + ":\tEND tearDown.");
	}
}