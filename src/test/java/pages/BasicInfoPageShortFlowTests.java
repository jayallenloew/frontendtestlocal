/*
 * Copyright Progressive Leasing LLC.
 */
package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.SessionId;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import common.FieldValidationUtilities;
import common.ProgressiveLeasingConstants;
import utilities.DeleteSessionCookie;
import utilities.DriverTypeLocal;
import utilities.ResetThisUser;
import utilities.TestBaseLocal;

/**
 * Test class for page object for the "Basic info" page, the new 
 * "short flow" (3 fields each).
 * <p>
 * <Note, all of these will fail when short flow is disabled in QA.)
 * <p>
 * Path: 
 * <p>
 * Store demo page > No Credit Needed<BR>Find out more | Marketing page > Get started | Request code page > Enter number, accept terms, Get started | Enter the code | Basic info
 * <p>
 * Version 1.0 2019-06-03
 * <p>
 * Version 2.0 2019-06-21 common setup
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @version 2.0
 * @see BasicInfoPageShortFlow.java
 */
public class BasicInfoPageShortFlowTests extends TestBaseLocal {

	private WebDriver driver;

	private BasicInfoPageShortFlow pageUnderTest;

	/**
	 * This value will be set during test execution. Then passed to 
	 * a cleanup utility during teardown.
	 */
	private String cookieToDelete;

	/**
	 * Test requires any one of these known valid test accounts; pick one at random.
	 */
	public static final String EMAIL_FOR_THIS_TEST = 
			ProgressiveLeasingConstants.TEST_DOT_COM_ADDRESSES[new java.util.Random().nextInt(ProgressiveLeasingConstants.TEST_DOT_COM_ADDRESSES.length)];

	/**
	 * Optional: override whatever driver type is currently declared in the superclass.
	 */
	@SuppressWarnings("unused")
	private static final DriverTypeLocal DRIVER_TYPE_OVERRIDE = 
			DriverTypeLocal.CHROME_HEADLESS;
	
	private static boolean isHappyPathNamePass = false;
	private static boolean isHappyPathDatePass = false;


	private void setUpLocal(String testNameIn) {

		System.out.println(testNameIn + ":\tsetup begin... (OK)");

		@SuppressWarnings("unused")
		ResetThisUser resetTemp = new ResetThisUser(EMAIL_FOR_THIS_TEST);
		FieldValidationUtilities.sleepSecond(testNameIn);
		resetTemp = null;

		/*
		 * Commented out (most of the time), you'll get whatever 
		 * is declared in the superclass.
		 */
//		setDriverTypeForTestClass(DRIVER_TYPE_OVERRIDE);
		
		driver = getPreparedDriver();

		// add fast-fail check for null driver here due to teardown
		Assert.assertNotNull(driver);
		System.out.println(testNameIn + ":\tdriver instantiated... (OK)");

		// go to demo page
		driver.navigate().to(ProgressiveLeasingConstants.URL_STORE_DEMO);

		new StoreDemoLandingPage(driver).getButtonFindOutMore().click();

		/*
		 * The redirects below are considered temporary.
		 * We'll do the "real" work under a separate 
		 * ticket after we've clarified requirements.
		 */
		
		// force redirect from 12 to 13
//		StringBuilder sBuild = new StringBuilder(driver.getCurrentUrl());
//		if(sBuild.toString().contains("qaswebapp12")) { 
//			System.out.println(testNameIn + ":\tbegin force redirect from 12 onto 13... (OK)");
//			int indexBegin = sBuild.toString().indexOf("qaswebapp");
//			sBuild.replace(indexBegin, (indexBegin+11), "qaswebapp13");
//			driver.navigate().to(sBuild.toString());
//			sBuild = null;
//		}

		// force redirect from 13 to 12
//		StringBuilder sBuild2 = new StringBuilder(driver.getCurrentUrl());
//		if(sBuild2.toString().contains("qaswebapp13")) { 
//			System.out.println(testNameIn + ":\tbegin force redirect from 13 onto 12... (OK)");
//			int indexBegin = sBuild2.toString().indexOf("qaswebapp");
//			sBuild2.replace(indexBegin, (indexBegin+11), "qaswebapp12");
//			driver.navigate().to(sBuild2.toString());
//			sBuild2 = null;
//		}


		// do not delete
		// wait for Get Started button 
		try { 
			new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[@type='cta']")));
			System.out.println(testNameIn + ":\tmarketing page... (OK)");
		}catch(TimeoutException tE) { 
			throw new AssertionError((testNameIn + ":\tfast-fail in setup; check marketing page."), tE);
		}

		new MarketingPage(driver).getButtonGet_started().click();
		
		GetStartedRequestCodePage requestCodeTemp = new GetStartedRequestCodePage(driver);
		requestCodeTemp.happyPathRequestCode(EMAIL_FOR_THIS_TEST);
		requestCodeTemp = null;

		new EnterTheCodePage(driver).enterThisCode("000000");
		
		// yeah the sleeps suck, thank Magento, move on....
		try { 
			Thread.sleep(8000); // Don't remove or reduce. It's not our issue.
		}catch(InterruptedException iE) { 
			throw new AssertionError((testNameIn + ":\tfast-fail in setup"), iE);
		}

		pageUnderTest = new BasicInfoPageShortFlow(driver);
		System.out.println(testNameIn + ":\t setup end....");
	}



	@Test(enabled=true,priority=1)
	public void testEnterName() { 

		String testNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();

		setUpLocal(testNameLocal);

		System.out.println(testNameLocal + ":\t begin test proper....");
				
		try { 
			pageUnderTest.enterName("Tester", "Lester");
			isHappyPathNamePass = true;
		}catch(Exception anyExceptionWhatsoever) { 
			throw new AssertionError(testNameLocal, anyExceptionWhatsoever);
		}
	}	
	
	
	@Test(enabled=true,priority=2)
	public void testEnterNameFirst() { 

		String testNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();

		if(isHappyPathNamePass) { 
			System.out.println(testNameLocal + ":\t skipped on purpose");
			throw new SkipException(testNameLocal);
		}
		
		setUpLocal(testNameLocal);

		System.out.println(testNameLocal + ":\t begin test proper....");
		
		WebElement fieldLocal = null;
		
		try { 
			fieldLocal = pageUnderTest.getFieldNameFirst();
		}catch(Exception anyExceptionWhatsoever) { 
			throw new AssertionError(testNameLocal, anyExceptionWhatsoever);			
		}
				
		try { 
			fieldLocal.sendKeys("Tester");
		}catch(Exception anyExceptionWhatsoever) { 
			throw new AssertionError(testNameLocal, anyExceptionWhatsoever);
		}finally { 
			fieldLocal = null;
		}
	}	
	
	
	
	@Test(enabled=true,priority=2)
	public void testEnterNameLast() { 

		String testNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();

		if(isHappyPathNamePass) { 
			System.out.println(testNameLocal + ":\t skipped on purpose");
			throw new SkipException(testNameLocal);
		}
		
		setUpLocal(testNameLocal);

		System.out.println(testNameLocal + ":\t begin test proper....");
		
		WebElement fieldLocal = null;
		
		try { 
			fieldLocal = pageUnderTest.getFieldNameLast();
		}catch(Exception anyExceptionWhatsoever) { 
			throw new AssertionError(testNameLocal, anyExceptionWhatsoever);			
		}
				
		try { 
			fieldLocal.sendKeys("Lester");
		}catch(Exception anyExceptionWhatsoever) { 
			throw new AssertionError(testNameLocal, anyExceptionWhatsoever);
		}finally { 
			fieldLocal = null;
		}
	}	
	

	@Test(enabled=true,priority=3)
	public void testEnterDate() { 

		String testNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();

		setUpLocal(testNameLocal);

		System.out.println(testNameLocal + ":\t begin test proper....");
				
		try { 
			pageUnderTest.enterDate("09", "10", "1984");
			System.out.println(testNameLocal + ":\tdate entered via happy path...(OK)\n");
		}catch(Exception anyExceptionWhatsoever) { 
			throw new AssertionError(testNameLocal, anyExceptionWhatsoever);
		}
		
		try { 
			Actions builder = new Actions(driver);
			builder.sendKeys(Keys.TAB).build().perform();
			builder = null;
			FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
			Assert.assertTrue(pageUnderTest.getDOBHappyCheckbox().isDisplayed());
			System.out.println(testNameLocal + ":\tPASS; happy path checkmark\n");
			isHappyPathDatePass = true;
		}catch(AssertionError aE) { 
			System.out.println(testNameLocal + ":\tFAIL\n");
			throw aE;
		}
	}	
	
	
	@Test(enabled=true,priority=4)
	public void testEnterDateMM() { 

		String testNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();

		if(isHappyPathDatePass) { 
			System.out.println(testNameLocal + ":\t skipped on purpose");
			throw new SkipException(testNameLocal);
		}
		
		setUpLocal(testNameLocal);

		System.out.println(testNameLocal + ":\t begin test proper....");
		
		WebElement fieldLocal = null;
		
		try { 
			fieldLocal = pageUnderTest.getFieldDate_MM();
		}catch(Exception anyExceptionWhatsoever) { 
			throw new AssertionError(testNameLocal, anyExceptionWhatsoever);			
		}
				
		try { 
			fieldLocal.sendKeys("09");
		}catch(Exception anyExceptionWhatsoever) { 
			throw new AssertionError(testNameLocal, anyExceptionWhatsoever);
		}finally { 
			fieldLocal = null;
		}
	}	
	
	
	@Test(enabled=true,priority=4)
	public void testEnterDateDD() { 

		String testNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();

		if(isHappyPathDatePass) { 
			System.out.println(testNameLocal + ":\t skipped on purpose");
			throw new SkipException(testNameLocal);
		}
		
		setUpLocal(testNameLocal);

		System.out.println(testNameLocal + ":\t begin test proper....");
		
		WebElement fieldLocal = null;
		
		try { 
			fieldLocal = pageUnderTest.getFieldDate_DD();
		}catch(Exception anyExceptionWhatsoever) { 
			throw new AssertionError(testNameLocal, anyExceptionWhatsoever);			
		}
				
		try { 
			fieldLocal.sendKeys("21");
		}catch(Exception anyExceptionWhatsoever) { 
			throw new AssertionError(testNameLocal, anyExceptionWhatsoever);
		}finally { 
			fieldLocal = null;
		}
	}		
	
	
	@Test(enabled=true,priority=4)
	public void testEnterDateYYYY() { 

		String testNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();

		if(isHappyPathDatePass) { 
			System.out.println(testNameLocal + ":\t skipped on purpose");
			throw new SkipException(testNameLocal);
		}
		
		setUpLocal(testNameLocal);

		System.out.println(testNameLocal + ":\t begin test proper....");
		
		WebElement fieldLocal = null;
		
		try { 
			fieldLocal = pageUnderTest.getFieldDate_YYYY();
		}catch(Exception anyExceptionWhatsoever) { 
			throw new AssertionError(testNameLocal, anyExceptionWhatsoever);			
		}
				
		try { 
			fieldLocal.sendKeys("1981");
		}catch(Exception anyExceptionWhatsoever) { 
			throw new AssertionError(testNameLocal, anyExceptionWhatsoever);
		}finally { 
			fieldLocal = null;
		}
	}		
	
	
	@SuppressWarnings("unused")
	private void captureSessionCookieToDelete() { 
		System.out.println("\n====cookie to console====");
		java.util.Set<Cookie> cookies = driver.manage().getCookies();
		for(Cookie C : cookies) { 
			if(!(C.getName().equals("sessionId"))) { 
				continue;
			}
			cookieToDelete = C.getValue();
			System.out.print("To delete:\t" + cookieToDelete);
		}
		System.out.println("\n====cookie to console====\n");
		cookies = null;
	}


	/**
	 * If subclass of TestBaseSauce, pass the test result up to the Sauce Labs Dashboard.
	 * <p>
	 * Quit driver.
	 * <p>
	 * Delete the cookie this test just created.
	 * <p>
	 * Session could potentially be null when everything's working fine, so we 
	 * do a null check here just to keep the results tab unpolluted. As of this 
	 * update, there should be one pass and one skip.
	 */
	@AfterMethod
	public void tearDown(ITestResult result) {
		String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
		SessionId session = ((RemoteWebDriver)driver).getSessionId();
		if(this.getClass().getSuperclass().getSimpleName().equals("TestBaseSauce")) { 
			System.out.println(methodNameLocal + ":\tSauce driver type detected; Sauce-specific teardown up next...");
			// could be null in a happy path run
			if(!(null==session)) { // don't use .equals() here
				((JavascriptExecutor) driver).executeScript("sauce:job-result=" + (result.isSuccess() ? "passed" : "failed"));
			}
		} else { 
			System.out.println(methodNameLocal + ":\tLocal driver type detected; Sauce-specific teardown skipped intentionally.\n");
		}
		if(!(null==driver)) { 
			driver.quit();
		}
		pageUnderTest = null;
		@SuppressWarnings("unused")
		DeleteSessionCookie deleteCookie = new DeleteSessionCookie(cookieToDelete,EMAIL_FOR_THIS_TEST);
		System.out.println("\n" + methodNameLocal + ":\tEND tearDown.");
	}
}
