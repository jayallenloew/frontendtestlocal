package fieldvalidations.getstartedrequestcodepage;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import common.FieldValidationUtilities;
import common.ProgressiveLeasingConstants;
import pages.GetStartedRequestCodePage;
import utilities.TestBaseLocal;
import waiter.Waiter;


/**
 * TestLink path:
 * <p>
 * General | Running | ECOM_UI | ECOM_FieldValidations | G-4116
 * <p>
 * G-4116:Field validation rules: Mobile phone
 * <p>
 * http://slc-qaswebtli01/testlink/linkto.php?tprojectPrefix=G&item=testcase&id=G-4116
 * <p>
 * GetStartedRequestCodePage.java > Mobile phone or email
 * <p>
 * Mobile phone is the only thing on this page for which there's any field validation.
 * <p>
 * Version 1.0 2019-01-15
 * <p>
 * Version 1.1 2019-01-22 parameterize local driver from suite file 
 * <p>
 * Version 2.0 2019-02-15 performance enhancements (thank you Shawn :-) )
 * <p>
 * @author <a href="mailto:jay.loew@progleasing.com">Allen Loew</a>
 * @author <a href="mailto:christopher.rains@progleasing.com">CJ Rains</a>
 * @version 2.0
 */
public class G_4116_MobilePhone extends TestBaseLocal {


	private Waiter waiter;
	private WebDriver driver;
	private GetStartedRequestCodePage pageUnderTest;
	private WebElement fieldUnderTest;
	private static final String getURLForThisClass() { 
		return ProgressiveLeasingConstants.URL_GET_STARTED_REQUEST_CODE_12;
	}
	
	/**
	 * Here we enter an apparently valid (for United States) 10-digit number and hope to see that happy green check mark.
	 * <p>
	 * @param method
	 */
	@Test(enabled=true)
	public void testHappyEntryValidPhone() {

		String testNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();

		waiter = new Waiter();

		driver = getPreparedDriver();
		
		waiter.get(getURLForThisClass(), driver, 15); 
		waiter.waitForPageLoadComplete(driver, 15);
		pageUnderTest = new GetStartedRequestCodePage(driver);
		fieldUnderTest = pageUnderTest.getFieldMobileOrEmail();

		String[] toPassIn = { "801-833-1234" , "8018331234" };

		for(String number : toPassIn) { 

			fieldUnderTest.clear();
			FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
			fieldUnderTest.sendKeys(number);
			FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
			
			Actions builder = new Actions(driver);
			builder.sendKeys(Keys.TAB).build().perform();
			FieldValidationUtilities.sleepSecond(testNameLocal);
			builder = null;

			WebElement happyCheckMark = driver.findElement(By.cssSelector("pg-icon.fg-green500.mode-default.default.pg-suffix-icon.pg-icon.material-icons"));

			try {
				Assert.assertTrue(happyCheckMark.isDisplayed());
				System.out.println("\n" + testNameLocal + ":\t" + "PASS with " + number);
			}catch(AssertionError aE) { 
				System.out.println("\n" + testNameLocal + ":\t" + "FAIL with " + number);
				throw aE;
			}finally { 
				happyCheckMark = null;
				builder = null;
				toPassIn = null;
				testNameLocal = null;
			}
		}
	}

	
	/**
	 * Somewhere a user is bound to begin typing area code with parentheses. This is not unreasonable. But it 
	 * is illegal, insofar as the application under test is concerned, and it should fail in a specified way.
	 * <p>
	 * Update 2019-03-26 disabled test intentionally as this functionality 
	 * is no longer present in the page under test. Instead, it drop-strips 
	 * the parentheses. So it's a non-issue.
	 * <p>
	 * @param method
	 */
	@Test(enabled=false)
	public void testParensRejected() {

		String testNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();

		waiter = new Waiter();

		driver = getPreparedDriver();
		
		waiter.get(getURLForThisClass(), driver, 15); 
		waiter.waitForPageLoadComplete(driver, 15);
		pageUnderTest = new GetStartedRequestCodePage(driver);
		fieldUnderTest = pageUnderTest.getFieldMobileOrEmail();

		String expected = 
				"Your mobile number can only contain 10 digits. Your email can't contain special " + 
						"characters other than at ( @ ), period ( . ), dash ( - ), underscore ( _ ) or plus ( + ).";

		fieldUnderTest.clear(); 
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		fieldUnderTest.sendKeys("(801)"); // begin with supposedly proper area code, which should be rejected
		pageUnderTest.getCheckboxTerms().click();
		FieldValidationUtilities.sleepSecond(testNameLocal);

		WebElement toGetTextFrom = driver.findElement(By.cssSelector(".pg-field-label-float .pg-field-error:nth-child(3)"));
		
		String captured = toGetTextFrom.getText();
		
		try {
			Assert.assertEquals(captured, expected);
			System.out.println("\n" + testNameLocal + ":\t" + "PASS");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ":\tFAIL");
			System.out.println(testNameLocal + ":\tExpected '" + expected + "\'");
			System.out.println(testNameLocal + ":\tCaptured '" + captured + "\'");
			throw aE;
		}finally { 
			expected = null;
			toGetTextFrom = null;
			testNameLocal = null;
			captured = null;
		}
	}	
	
	


	/**
	 * Here we enter a 7-digit number, obviously not a valid phone number, probably a mistaken tab-out.
	 * <p>
	 * @param method
	 */
	@Test(enabled=true)
	public void testFewerThanTen() {

		String testNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();

		waiter = new Waiter();

		driver = getPreparedDriver();
		
		waiter.get(getURLForThisClass(), driver, 15); 
		waiter.waitForPageLoadComplete(driver, 15);
		pageUnderTest = new GetStartedRequestCodePage(driver);
		fieldUnderTest = pageUnderTest.getFieldMobileOrEmail();

		String toPassIn = "801-999-9";

		String expected = "You must enter a valid phone or email";

		fieldUnderTest.clear();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		fieldUnderTest.sendKeys(toPassIn);
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		pageUnderTest.getCheckboxTerms().click();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		WebElement toGetTextFrom = driver.findElement(By.cssSelector(".pg-error:nth-child(3)"));

		String captured = toGetTextFrom.getText();
		
		try {
			Assert.assertEquals(captured, expected);
			System.out.println("\n" + testNameLocal + ":\t" + "PASS");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ":\tFAIL");
			System.out.println(testNameLocal + ":\tExpected '" + expected + "\'");
			System.out.println(testNameLocal + ":\tCaptured '" + captured + "\'");
			throw aE;
		}finally { 
			expected = null;
			toGetTextFrom = null;
			testNameLocal = null;
			captured = null;
		}
	}



	
	
	/**
	 * Here we enter a 3-digit number, obviously not a valid phone number, but possibly 
	 * an area code followed by a mistaken tab-out.
	 * <p>
	 * @param method
	 */
	@Test(enabled=true)
	public void testThreeDigitInteger() {

		String testNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();

		waiter = new Waiter();

		driver = getPreparedDriver();
		
		waiter.get(getURLForThisClass(), driver, 15); 
		waiter.waitForPageLoadComplete(driver, 15);
		pageUnderTest = new GetStartedRequestCodePage(driver);
		fieldUnderTest = pageUnderTest.getFieldMobileOrEmail();

		String toPassIn = "123";

		String expected = "You must enter a valid phone or email";

		fieldUnderTest.clear();
		
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		fieldUnderTest.sendKeys(toPassIn);
		
		FieldValidationUtilities.sleepSecond(testNameLocal);

		pageUnderTest.getCheckboxTerms().click();

		FieldValidationUtilities.sleepSecond(testNameLocal);

		WebElement toGetTextFrom = driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='This field is required'])[1]/following::span[1]"));

		String captured = toGetTextFrom.getText();

		try {
			Assert.assertEquals(captured, expected);
			System.out.println("\n" + testNameLocal + ":\t" + "PASS");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ":\tFAIL");
			System.out.println(testNameLocal + ":\tExpected '" + expected + "\'");
			System.out.println(testNameLocal + ":\tCaptured '" + captured + "\'");
			throw aE;
		}finally { 
			toPassIn = null;
			expected = null;
			toGetTextFrom = null;
			testNameLocal = null;
			captured = null;
		}
	}

	
	/**
	 * If subclass of TestBaseSauce, pass the test result up to the Sauce Labs Dashboard.
	 * <p>
	 * Quit driver.
	 * <p>
	 * Delete the cookie this test just created.
	 */
	@AfterMethod
	public void tearDown(ITestResult result) throws Exception {
		String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
		if(this.getClass().getSuperclass().getSimpleName().equals("TestBaseSauce")) { 
			System.out.println(methodNameLocal + ":\tSauce driver type detected; Sauce-specific teardown up next...");
			((JavascriptExecutor) driver).executeScript("sauce:job-result=" + (result.isSuccess() ? "passed" : "failed"));
		} else { 
			System.out.println(methodNameLocal + ":\tLocal driver type detected; Sauce-specific teardown skipped intentionally.\n");
		}
		driver.quit();
		waiter = null;
		pageUnderTest = null;
		fieldUnderTest = null;
		System.out.println(methodNameLocal + ":\tEND tearDown.");
	}
}
